#' @import bnutil
#' @import ggplot2
#' @export
shinyServerRun = function(input, output, session, context) {

  getFolderReactive = context$getRunFolder()
  getDataReactive = context$getData()
  getSettingsReactive = context$getFolder()

  output$body = renderUI({
    tabsetPanel(
      tabPanel("Settings",
               fluidPage(
                 wellPanel(
                   checkboxInput("twosamp", "Two sample T-test", TRUE),
                   conditionalPanel(
                     condition = "input.twosamp == true",
                     selectInput("group", "Grouping Factor", choices = list(), multiple = TRUE)),
                   conditionalPanel(
                     condition = "input.twosamp == true",
                     checkboxInput("ispaired", "Paired T-test", FALSE)),
                   conditionalPanel(
                     condition = "input.ispaired == true",
                     selectInput("pairing", "Pairing Factor", choices = list(), multiple = TRUE)
                   ),
                   conditionalPanel(
                     condition = "input.twosamp == true",
                     checkboxInput("varequal", "Assume equal variance in both groups (recommended)", TRUE)
                   )
                 ),
                 wellPanel(
                   selectInput("alternative", "Alternative hypothesis", list("two.sided", "greater", "less")),
                   checkboxInput("invert", "Inverse the sign of the effect (delta) in output", FALSE)
                 ),
                 wellPanel(
                   actionButton("start", "Start the analysis"),
                   tags$hr(),
                   verbatimTextOutput("status")
                 )

               )
      ),
      tabPanel("Preview",
               sliderInput("cell", "Scroll",value = 1, step =1, min = 1, max = 2),
               plotOutput("explot"),
               tableOutput("extest")
      )

    )
  })

  observe({
    getFolder = getFolderReactive$value
    if (is.null(getFolder)) return()
    folder = getFolder()

    getData=getDataReactive$value
    if (is.null(getData)) return()
    bndata = getData()

    getSettings = getSettingsReactive$value
    if (is.null(getSettings)) return()
    settingsFile = file.path(getSettings(), "settings.RData")

    labelList = c()
    columnNameList = c()
    if(bndata$hasXAxis){
      labelList = c(labelList, bndata$xAxisLabel)
      columnNameList = c(columnNameList, bndata$xAxisColumnName)
    }
    if(bndata$hasColors){
      labelList = c(labelList, bndata$colorLabels)
      columnNameList = c(columnNameList, bndata$colorColumnNames)
    }
    names(columnNameList) = labelList
    updateSelectInput(session, "group", choices = columnNameList)
    updateSelectInput(session, "pairing", choices = columnNameList)

    if(bndata$hasColors){
      updateSelectInput(session, "group", selected = bndata$colorLabels)
    }

    if(bndata$hasXAxis){
      updateSelectInput(session, "pairing", selected = bndata$xAxisLabel)
    }

    observeEvent(input$twosamp, {
        if (!input$twosamp){
          updateCheckboxInput(session, "ispaired", value = FALSE)
        }
    })

    updateSliderInput(session, "cell", max = max(bndata$data$ids))

    if(file.exists(settingsFile)){
      s = load(settingsFile)
      updateCheckboxInput(session, "twosamp", value = settingsList$twosamp)
      updateSelectInput(session, "group", selected =  settingsList$group)
      updateCheckboxInput(session, "ispaired", value = settingsList$ispaired)
      updateSelectInput(session, "pairing", selected = settingsList$pairing)
      updateCheckboxInput(session, "varequal", value = settingsList$varequal)
      updateSelectInput(session, "alternative", selected = settingsList$alternative)
      updateCheckboxInput(session, "invert", value = settingsList$invert)
    }

    getTestType = reactive({
      if(input$twosamp){
        if(input$ispaired){
          return("paired")
        }else{
          return("unpaired")
        }
      }else{
        return("onesamp")
      }
    })

    getTestFun = reactive({
      if(getTestType() == "paired"){
        tFun = t2s_paired
        return(tFun)
      }
      if(getTestType() == "unpaired"){
        tFun = t2s_unpaired
        return(tFun)
      }
      if(getTestType() == "onesamp")
      {
        tFun = t1s
        return(tFun)
      }
      return(NULL)
    })

    prepdf = reactive({
      df = bndata$data
      if(getTestType() == "paired"){
        if(is.null(input$group)){
          stop("Grouping factor not defined")
        } else {
          grp = droplevels(interaction(df[[input$group]]))
        }
        if(is.null(input$pairing)){
          stop("Pairing factor not defined")
        } else {
          pairing = droplevels(interaction(df[[input$pairing]]))
        }
        df = data.frame(df, grouping = grp, pairing = pairing)
      }
      if(getTestType() == "unpaired"){
        if(is.null(input$group)){
          stop("Grouping factor not defined")
        } else {
          grp = droplevels(interaction(df[[input$group]]))
        }
        df = data.frame(df, grouping = grp)
      }
      return(df)
    })

    getSettingsList = reactive({
      settingsList = list(twosamp = input$twosamp,
                          group = input$group,
                          ispaired = input$ispaired,
                          pairing = input$pairing,
                          varequal = input$varequal,
                          alternative = input$alternative,
                          invert = input$invert)
    })

    output$explot = renderPlot({
      exdf = prepdf()
      exdf = subset(exdf, ids == input$cell)
      if(getTestType() == "paired"){
        prt = ggplot(exdf, aes(x = grouping, y = value, colour = pairing)) + geom_point()
        return(print(prt))
      }
      if(getTestType() == "unpaired"){
        prt = ggplot(exdf, aes(x = grouping, y = value)) + geom_point()
        return(print(prt))
      }
      if (getTestType() == "onesamp"){
        exdf = data.frame(exdf, index = 1:dim(exdf)[1])
        prt = ggplot(exdf, aes(x = index, y = value)) + geom_point()
        print(prt)
      }
    })

    output$extest = renderTable({
      exdf = prepdf()
      exdf = subset(exdf, ids == input$cell)
      tFun = getTestFun()
      settingsList = getSettingsList()
      aTest = tFun(exdf, settingsList)
      if(bndata$hasSpots){
        spots = exdf[bndata$spotColumnNames]
        cnames = colnames(aTest)
        aTest = cbind(spots[1,], aTest)
        colnames(aTest) = c(colnames(spots), cnames)
      }
      if(bndata$hasArrays){
        arrays = exdf[bndata$arrayColumnNames]
        cnames = colnames(aTest)
        aTest = cbind(arrays[1,], aTest)
        colnames(aTest) = c(colnames(arrays), cnames)
      }

      return(aTest)
    })

    output$status = renderText({
      if (input$start > 0){
        isolate({
          settingsList = getSettingsList()
          save(file = settingsFile, settingsList)
          df = prepdf()
          #collect any column info
          if(bndata$hasArrays){
            df = data.frame(df, arrayLabel  = droplevels(interaction(df[bndata$arrayColumnNames] )) )
          } else {
            df = data.frame(df, arrayLabel = df$colSeq)
          }

          if(bndata$hasSpots){
            df = data.frame(df, spotLabel = droplevels(interaction(df[[bndata$spotColumnNames]])) )
          }else{
            df = data.frame(df, spotLabel = df$rowSeq)
          }
          tFun = getTestFun()
          tResult = df %>% group_by(rowSeq, colSeq, arrayLabel, spotLabel) %>% do(tFun(., settingsList))
          tResult = tResult %>% group_by(arrayLabel) %>% mutate(fdr = p.adjust(p, method = "fdr"))

          tResult$logp = -log10(tResult$p)
          runFile = file.path(getFolder(), "runFile.RData")
          save(file = runFile, tResult)
          tResult = subset(tResult, select = -c(arrayLabel, spotLabel))
          meta = data.frame(labelDescription = colnames(tResult), groupingType = c("rowSeq","colSeq",
                                                                                   "QuantitationType",
                                                                                   "QuantitationType",
                                                                                   "QuantitationType",
                                                                                   "QuantitationType",
                                                                                   "QuantitationType"))
          result = AnnotatedData$new(data = tResult, metadata = meta)
          context$setResult(result)
          return("Done")
        })
      }
      else{
        return(".")
      }

    })

  })
}
#' @import scales
#' @export
shinyServerShowResults = function(input, output, session, context){
  getFolderReactive = context$getRunFolder()
  getDataReactive = context$getData()
  output$body = renderUI({
    fluidPage(
        tags$h1("Volcano Plot"),
        helpText("Move the mouse over the points to see more information."),
        helpText("Note that the coloring of the points indicates the false discovery rate (FDR).")
      ,
      fluidRow(
        plotOutput("volcano", height = "600px",  hover = hoverOpts(id="plot_hover") )
      ),
      fluidRow(
        tableOutput("hover_info")
      )
    )
  })

  observe({
    getFolder = getFolderReactive$value
    if (is.null(getFolder)) return()
    runFile = file.path(getFolder(), "runFile.RData")
    load(file = runFile)
    tResult = subset(tResult, !is.na(p) & !is.na(delta))
    output$volcano = renderPlot({
      vol = ggplot(tResult, aes(x = delta, y = logp, colour = fdr)) + geom_point() + facet_wrap(~arrayLabel)
      vol = vol + xlab("Delta") + ylab("Significance")
      vol = vol + scale_colour_gradientn(name = "FDR",space = "rgb",
                             colours = c("green", "white", "black"),
                             values = c(0,0.125,1),
                             limits = c(0,1))
      vol = vol + theme(panel.background = element_rect(fill = muted('blue'), colour = 'black'))
      return(vol)
    })

    output$hover_info = renderTable({
      if(!is.null(input$plot_hover)){
        hvr = input$plot_hover
        df = subset(tResult, arrayLabel == hvr$panelvar1)
        dx = (hvr$x-df$delta)/diff(range(df$delta))
        dy = (hvr$y-df$logp) /diff(range(df$logp))
        d = sqrt(dx^2 + dy^2)
        if ( min(d) < 0.03){
          idx = which.min(d)
          out = tResult[idx,]
          out$panel = hvr$panelvar1
          out = subset(out, select = c(panel, spotLabel, delta, p, fdr))
        }
      }
    })
  })
}
