checkFun = function(val){
  if(length(val) == 0) return(NA)
  if(length(val) == 1) return(val)
  if(length(val) > 1 ) stop("Pairing/grouping not consistent for paired t-test")
}
